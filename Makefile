##
## Makefile for libmy in /home/chartier/rendu/libmy
##
## Made by CHARTIER Rodolphe
## Login   <charti_r@etna-alternance.net>
##
## Started on  Thu Oct 12 17:19:52 2017 CHARTIER Rodolphe
## Last update Thu Oct 12 17:25:51 2017 CHARTIER Rodolphe
##

CFLAGS		+= -Wall -Wextra -Werror
CFLAGS		+= -I ./header/

RM		= rm -f

NAME		= libmy.a

SRC_DIR		= source/

SRC		= \
		$(SRC_DIR)my_clear_str.c \
		$(SRC_DIR)my_fprintf.c \
		$(SRC_DIR)my_fputchar.c \
		$(SRC_DIR)my_fputnbr_base.c \
		$(SRC_DIR)my_fputnbr.c \
		$(SRC_DIR)my_fputstr.c \
		$(SRC_DIR)my_file.c \
		$(SRC_DIR)my_getnbr_base.c \
		$(SRC_DIR)my_getnbr.c \
		$(SRC_DIR)my_printf.c \
		$(SRC_DIR)my_putchar.c \
		$(SRC_DIR)my_putnbr_base.c \
		$(SRC_DIR)my_putnbr.c \
		$(SRC_DIR)my_putstr.c \
		$(SRC_DIR)my_sizeof_char.c \
		$(SRC_DIR)my_sizeof_line.c \
		$(SRC_DIR)my_sizeof_word.c \
		$(SRC_DIR)my_sizeof_nbrbase.c \
		$(SRC_DIR)my_sizeof_nbr.c \
		$(SRC_DIR)my_sort_int_tab.c \
		$(SRC_DIR)my_strlen.c \
		$(SRC_DIR)my_str_realloc.c \
		$(SRC_DIR)my_str_to_wordtab.c \
		$(SRC_DIR)my_swap.c \
		$(SRC_DIR)my_charcmp.c \
		$(SRC_DIR)my_jump.c \
		$(SRC_DIR)my_memset.c \
		$(SRC_DIR)my_revstr.c \
		$(SRC_DIR)my_showstr.c \
		$(SRC_DIR)my_strcapitalize.c \
		$(SRC_DIR)my_strcat.c \
		$(SRC_DIR)my_strcmp.c \
		$(SRC_DIR)my_strcpy.c \
		$(SRC_DIR)my_str_isalpha.c \
		$(SRC_DIR)my_str_islower.c \
		$(SRC_DIR)my_str_isnum.c \
		$(SRC_DIR)my_str_isprintable.c \
		$(SRC_DIR)my_str_isupper.c \
		$(SRC_DIR)my_strlcat.c \
		$(SRC_DIR)my_strlcpy.c \
		$(SRC_DIR)my_strlowcase.c \
		$(SRC_DIR)my_strncat.c \
		$(SRC_DIR)my_strncmp.c \
		$(SRC_DIR)my_strncpy.c \
		$(SRC_DIR)my_strstr.c \
		$(SRC_DIR)my_strupcase.c \
		$(SRC_DIR)my_find_primeup.c \
		$(SRC_DIR)my_is_prime.c \
		$(SRC_DIR)my_nbr_isneg.c \
		$(SRC_DIR)my_pow.c \
		$(SRC_DIR)my_sqrt.c

OBJ		= $(SRC:.c=.o)

all: $(NAME)

$(NAME): $(OBJ)
	ar rcs $(NAME) $(OBJ)

clean:
	$(RM) $(OBJ)

fclean: clean
	$(RM) $(NAME)

re: fclean all

test: all
	make -C cor/
	gcc -g3 cor/main.c $(NAME) -o my_test
	gcc -g3 cor/main.c cor/cor_$(NAME) -o cor_test

.PHONY: all clean fclean re test
