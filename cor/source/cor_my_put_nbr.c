#include <string.h>

void my_putchar( char );

void _put( int nb, char *base, int baselen ) {
	if ( nb <= -baselen ) {
		_put( nb / baselen, base, baselen );
	}
	my_putchar( base[ -( nb % baselen ) ] );
}

void my_put_nbr_base( int nb, char *base ) {
	if ( nb < 0 ) {
		my_putchar( '-' );
	} else {
		nb = -nb;
	}
	_put( nb, base, strlen( base ) );
}

void my_put_nbr( int n ) {
	my_put_nbr_base( n, "0123456789" );
}
