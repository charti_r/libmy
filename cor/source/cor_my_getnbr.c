#include <string.h>

int _get_nb_sign( char** nb ) {
	char c;
	int sign = 1;

	for ( ; ( c = **nb ); ++*nb ) {
		if ( c == '-' ) {
			sign = -sign;
		} else if ( c != '+' ) {
			break;
		}
	}
	return sign;
}

int my_getnbr_base( char *nb, char *base ) {
	char* pos;
	int i = 0,
		res = 0,
		sign = _get_nb_sign( &nb ),
		len = strlen( nb ),
		baselen = strlen( base );

	for ( ; i < len && ( pos = strchr( base, nb[ i ] ) ); ++i ) {
		res *= baselen;
		res += ( pos - base ) * sign;
	}
	return res;
}

int my_getnbr( char *s ) {
	return my_getnbr_base( s, "0123456789" );
}
