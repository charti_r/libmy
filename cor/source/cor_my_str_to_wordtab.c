#include <ctype.h>
#include <string.h>
#include <stdlib.h>
#include <stdio.h>

static char** _strsplit( const char* s, const char* del ) {
	void* data;
	char* _s = ( char* )s;
	const char** ptrs;
	unsigned int
		ptrsSize,
		sLen = strlen( s ),
		delLen = strlen( del ),
		nbWords = 1;

	while ( ( _s = strstr( _s, del ) ) ) {
		_s += delLen;
		++nbWords;
	}
	ptrsSize = ( nbWords + 1 ) * sizeof( char* );
	ptrs = data = malloc( ptrsSize + sLen + 1 );
	if ( data ) {
		*ptrs = _s = strcpy( ( ( char* )data ) + ptrsSize, s );
		if ( nbWords > 1 ) {
			while ( ( _s = strstr( _s, del ) ) ) {
				*_s = '\0';
				_s += delLen;
				*++ptrs = _s;
			}
		}
		*++ptrs = NULL;
	}
	return data;
}

char** my_str_to_wordtab( char* s ) {
	char** arr = NULL;
	char* _s = malloc( strlen( s ) + 1 );
	char* _save = _s;

	if ( _s ) {
		for ( ; *s; ++s ) {
			if ( isalnum( *s ) ) {
				*_s = *s;
				++_s;
			} else if ( _s > _save && isalnum( s[ 1 ] ) ) {
				*_s = ' ';
				++_s;
			}
		}
		*_s = '\0';
		if ( *_save != '\0' ) {
			arr = _strsplit( _save, " " );
		} else if ( ( arr = malloc( sizeof *arr ) ) ) {
			*arr = NULL;
		}
		free( _save );
	}
	return arr;
}
