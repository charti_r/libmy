#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

void my_putchar( char );
int my_isneg( int );
int my_put_nbr( int );
void my_swap( int*, int* );
int my_putstr( char* );
int my_strlen( char* );
int my_getnbr( char* );
char* my_strcpy( char* dest, char* src );
char* my_strncpy( char* dest, char* src, int n );
int my_strcmp( char* s1, char* s2 );
int my_strncmp( char* s1, char* s2, int n );
char* my_strcat( char* s1, char* s2 );
char* my_strncat( char* s1, char* s2, int n );
char* my_strstr( char* str, char* to_find );

char* my_strdup( char* );
char** my_str_to_wordtab( char* );

void _putchar( char c ) {
	write( 1, "my_putchar. ", 12 );
	my_putchar( c );
	write( 1, "\n", 1 );
}

void _isneg( int n ) {
	printf( "my_isneg. %d : %d\n", n, my_isneg( n ) );
}

void _swap( int a, int b ) {
	my_swap( &a, &b );
	printf( "my_swap. %d <-> %d\n", a, b );
}

void _putstr( char* s ) {
	my_putstr( "my_putstr. [" );
	my_putstr( s );
	my_putstr( "]\n" );
}

void _put_nbr( int n ) {
	write( 1, "my_put_nbr. ", 12 );
	my_put_nbr( n );
	my_putchar( '\n' );
}

void _getnbr( char* s ) {
	printf( "my_getnbr. (\"%s\") -> %d\n", s, my_getnbr( s ) );
}

void _strcat( char* dest, char* src ) {
	char s[ 128 ] = { '\0' };

	strcpy( s, dest );
	printf( "my_strcat. [%s] + [%s] = [%s]\n", dest, src, my_strcat( s, src ) );
}

void _strcmp( char* s1, char* s2 ) {
	int res = my_strcmp( s1, s2 );

	printf( "my_strcmp. [%s] %c [%s]\n", s1, res ? res < 0 ? '<' : '>' : '=', s2 );
}

void _strcpy( char* src ) {
	char s[ 512 ] = { '\0' };

	printf( "my_strcpy. [%s]\n", my_strcpy( s, src ) );
}

void _strlen( char* s ) {
	printf( "my_strlen. %d\n", my_strlen( s ) );
}

void _strncat( char* dest, char* src, int n ) {
	char s[ 128 ] = { '\0' };

	strcpy( s, dest );
	printf( "my_strncat. [%s] +(%d) [%s] = [%s]\n", dest, n, src, my_strncat( s, src, n ) );
}

void _strncmp( char* s1, char* s2, int n ) {
	int res = my_strncmp( s1, s2, n );

	printf( "my_strncmp. [%s] %c(%d) [%s]\n", s1, res ? res < 0 ? '<' : '>' : '=', n, s2 );
}

void _strncpy( char* src, int n ) {
	char s[ 512 ] = { '\0' };

	printf( "my_strncpy. [%s] ->(%d) [%s]\n", src, n, my_strncpy( s, src, n ) );
}

void _strstr( char* haystack, char* needle ) {
	char* ret = my_strstr( haystack, needle );

	printf( "my_strstr. (\"%s\", \"%s\") -> [%s](%ld)\n",
		haystack, needle,
		ret ? ret : "NULL",
		ret ? ret - haystack : -1 );
}

void _strdup( char* s ) {
	char* sdup = my_strdup( s );

	printf( "my_strdup. [%s] [%s] [%d]\n", s, sdup, s != sdup );
}

void _str_to_wordtab( char* s ) {
	char** arr = my_str_to_wordtab( s );
	char** _arr = arr;

	if ( arr ) {
		printf( "my_str_to_wordtab. \"%s\" -> [", s );
		if ( *arr ) {
			printf( "\"%s\"", *arr );
			while ( *++arr ) {
				printf( ", \"%s\"", *arr );
			}
		}
		printf( "]\n" );
		free( _arr );
	}
}

int main( void ) {
	_putchar( 'G' );
	_putchar( 'o' );
	_putchar( '!' );

	_isneg( 0 );
	_isneg( 1 );
	_isneg( -1 );
	_isneg( 10 );
	_isneg( -10 );
	_isneg( 2147483647 );
	_isneg( -2147483647 );
	_isneg( -2147483648 );

	_putstr( "" );
	_putstr( "a" );
	_putstr( "abcdef" );
	_putstr( "Lorem ipsum dolor sit amet" );

	_strcat( "", "" );
	_strcat( "a", "" );
	_strcat( "", "a" );
	_strcat( "a", "b" );
	_strcat( "a", "bc" );
	_strcat( "ab", "cd" );
	_strcat( "Hello", " world!" );
	_strcat( "ABCDEFG", "HIJKLMNOPQRSTUVWXYZ" );

	_strcmp( "", "" );
	_strcmp( "", "1" );
	_strcmp( "1", "" );
	_strcmp( "0", "1" );
	_strcmp( "1", "0" );
	_strcmp( "1", "1" );
	_strcmp( "abcdefghijklmnopqr", "abcdefghijklmnopqrstuvwxyz" );
	_strcmp( "abcdefghijklmnopqrstuvwxyz", "abcdefghijklmnopqr" );
	_strcmp( "abcdefghijklmnopqrstuvwxyz", "abcdefghijklmnopqrstuvwxyz" );
	_strcmp( "abcdefghijklmnopqrstuvwxyz", "abcdefghijklMNOpqrstuvwxyz" );
	_strcmp( "abcdefghijklMNOpqrstuvwxyz", "abcdefghijklmnopqrstuvwxyz" );
	_strcmp( "abcdefghijklMNOpqrstuvwxyz", "stuvwxyzabcdefghijklmnopqr" );
	_strcmp( "stuvwxyzabcdefghijklmnopqr", "abcdefghijklMNOpqrstuvwxyz" );

	_strcpy( "" );
	_strcpy( "a" );
	_strcpy( "abc" );
	_strcpy( "Lorem ipsum dolor sit amet etc." );

	_strlen( "" );
	_strlen( "a" );
	_strlen( "Hello World" );
	_strlen( "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum." );

	_strncat( "", "", 0 );
	_strncat( "", "", 1 );
	_strncat( "a", "", 999 );
	_strncat( "", "a", 0 );
	_strncat( "", "a", 1 );
	_strncat( "", "a", 999 );
	_strncat( "a", "b", 0 );
	_strncat( "a", "b", 1 );
	_strncat( "a", "b", 999 );
	_strncat( "a", "bc", 0 );
	_strncat( "a", "bc", 1 );
	_strncat( "a", "bc", 999 );
	_strncat( "ab", "cd", 0 );
	_strncat( "ab", "cd", 1 );
	_strncat( "ab", "cd", 999 );
	_strncat( "Hello", " world!", 0 );
	_strncat( "Hello", " world!", 1 );
	_strncat( "Hello", " world!", 2 );
	_strncat( "Hello", " world!", 6 );
	_strncat( "Hello", " world!", 7 );
	_strncat( "Hello", " world!", 8 );
	_strncat( "Hello", " world!", 9 );
	_strncat( "Hello", " world!", 10 );
	_strncat( "Hello", " world!", 999 );
	_strncat( "ABCDEFG", "HIJKLMNOPQRSTUVWXYZ", 0 );
	_strncat( "ABCDEFG", "HIJKLMNOPQRSTUVWXYZ", 1 );
	_strncat( "ABCDEFG", "HIJKLMNOPQRSTUVWXYZ", 2 );
	_strncat( "ABCDEFG", "HIJKLMNOPQRSTUVWXYZ", 5 );
	_strncat( "ABCDEFG", "HIJKLMNOPQRSTUVWXYZ", 10 );
	_strncat( "ABCDEFG", "HIJKLMNOPQRSTUVWXYZ", 17 );
	_strncat( "ABCDEFG", "HIJKLMNOPQRSTUVWXYZ", 18 );
	_strncat( "ABCDEFG", "HIJKLMNOPQRSTUVWXYZ", 19 );
	_strncat( "ABCDEFG", "HIJKLMNOPQRSTUVWXYZ", 20 );
	_strncat( "ABCDEFG", "HIJKLMNOPQRSTUVWXYZ", 21 );
	_strncat( "ABCDEFG", "HIJKLMNOPQRSTUVWXYZ", 999 );

	_strncmp( "", "", 0 );
	_strncmp( "", "", 1 );
	_strncmp( "", "", 999 );
	_strncmp( "", "1", 0 );
	_strncmp( "", "1", 1 );
	_strncmp( "", "1", 999 );
	_strncmp( "1", "", 0 );
	_strncmp( "1", "", 1 );
	_strncmp( "1", "", 999 );
	_strncmp( "0", "1", 0 );
	_strncmp( "0", "1", 1 );
	_strncmp( "0", "1", 999 );
	_strncmp( "1", "0", 0 );
	_strncmp( "1", "0", 1 );
	_strncmp( "1", "0", 999 );
	_strncmp( "1", "1", 0 );
	_strncmp( "1", "1", 1 );
	_strncmp( "1", "1", 999 );
	_strncmp( "0123456789", "0123_56789",  0 );
	_strncmp( "0123456789", "0123_56789",  1 );
	_strncmp( "0123456789", "0123_56789",  3 );
	_strncmp( "0123456789", "0123_56789",  4 );
	_strncmp( "0123456789", "0123_56789",  5 );
	_strncmp( "0123456789", "0123_56789",  6 );
	_strncmp( "0123456789", "0123_56789", 10 );
	_strncmp( "0123456789", "0123_56789", 99 );
	_strncmp( "0123456789", "0123456789A",  0 );
	_strncmp( "0123456789", "0123456789A",  1 );
	_strncmp( "0123456789", "0123456789A",  8 );
	_strncmp( "0123456789", "0123456789A",  9 );
	_strncmp( "0123456789", "0123456789A", 10 );
	_strncmp( "0123456789", "0123456789A", 11 );
	_strncmp( "0123456789", "0123456789A", 12 );
	_strncmp( "0123456789", "0123456789A", 99 );
	_strncmp( "", "0123456789A", 0 );
	_strncmp( "", "0123456789A", 1 );
	_strncmp( "", "0123456789A", 99 );
	_strncmp( "0", "0123456789A", 0 );
	_strncmp( "0", "0123456789A", 1 );
	_strncmp( "0", "0123456789A", 99 );
	_strncmp( "abcdefghijklmnopqr", "abcdefghijklmnopqrstuvwxyz", 0 );
	_strncmp( "abcdefghijklmnopqr", "abcdefghijklmnopqrstuvwxyz", 1 );
	_strncmp( "abcdefghijklmnopqr", "abcdefghijklmnopqrstuvwxyz", 17 );
	_strncmp( "abcdefghijklmnopqr", "abcdefghijklmnopqrstuvwxyz", 18 );
	_strncmp( "abcdefghijklmnopqr", "abcdefghijklmnopqrstuvwxyz", 19 );
	_strncmp( "stuvwxyzabcdefghijklmnopqr", "abcdefghijklMNOpqrstuvwxyz", 0 );
	_strncmp( "stuvwxyzabcdefghijklmnopqr", "abcdefghijklMNOpqrstuvwxyz", 1 );
	_strncmp( "stuvwxyzabcdefghijklmnopqr", "abcdefghijklMNOpqrstuvwxyz", 99 );

	_strncpy( "", 0 );
	_strncpy( "", 1 );
	_strncpy( "", 2 );
	_strncpy( "", 512 );
	_strncpy( "a", 0 );
	_strncpy( "a", 1 );
	_strncpy( "a", 2 );
	_strncpy( "a", 512 );
	_strncpy( "abc", 0 );
	_strncpy( "abc", 1 );
	_strncpy( "abc", 3 );
	_strncpy( "abc", 4 );
	_strncpy( "abc", 512 );
	_strncpy( "Lorem ipsum dolor sit amet etc.", 0 );
	_strncpy( "Lorem ipsum dolor sit amet etc.", 1 );
	_strncpy( "Lorem ipsum dolor sit amet etc.", 5 );
	_strncpy( "Lorem ipsum dolor sit amet etc.", 30 );
	_strncpy( "Lorem ipsum dolor sit amet etc.", 31 );
	_strncpy( "Lorem ipsum dolor sit amet etc.", 32 );
	_strncpy( "Lorem ipsum dolor sit amet etc.", 512 );

	_strstr( "", "" );
	_strstr( "", "a" );
	_strstr( "a", "" );
	_strstr( "a", "a" );
	_strstr( "a", "b" );
	_strstr( "ab", "b" );
	_strstr( "qwe asd zxc 123 456 789", "123" );
	_strstr( "qwe asd zxc 123 456 789", "789" );
	_strstr( "qwe asd zxc 123 456 789", "123 456 789" );
	_strstr( "qwe asd zxc 123 456 789", "qwe asd zxc 123 456 789" );
	_strstr( "qwe asd zxc 123 456 789", "qwe asd zxc 123 456 789_" );
	_strstr( "qwe asd zxc 123 456 789_", "qwe asd zxc 123 456 789" );
	_strstr( "", "qwe asd zxc 123 456 789" );
	_strstr( "a", "qwe asd zxc 123 456 789" );
	_strstr( "qwe asd", "qwe asd zxc 123 456 789" );
	_strstr( "qwe qwe qwe 123 456 789", "qwe" );
	_strstr( "123 qwe qwe qwe 456 789", "qwe" );
	_strstr( "123 qwe qwe qwe 456 789", "" );
	_strstr( "123 qwe qwe qwe 456 789", "q" );

	_put_nbr( 0 );
	_put_nbr( 1 );
	_put_nbr( 9 );
	_put_nbr( 10 );
	_put_nbr( 21 );
	_put_nbr( 99 );
	_put_nbr( 999 );
	_put_nbr( 1000 );
	_put_nbr( 987654321 );
	_put_nbr( 1000000000 );
	_put_nbr( 1234567890 );
	_put_nbr( 2000000000 );
	_put_nbr( 2147483647 );
	_put_nbr( -1 );
	_put_nbr( -9 );
	_put_nbr( -10 );
	_put_nbr( -21 );
	_put_nbr( -99 );
	_put_nbr( -999 );
	_put_nbr( -1000 );
	_put_nbr( -987654321 );
	_put_nbr( -1000000000 );
	_put_nbr( -1234567890 );
	_put_nbr( -2000000000 );
	_put_nbr( -2147483647 );
	_put_nbr( -2147483648 );

	_getnbr( "" );
	_getnbr( "0" );
	_getnbr( "1" );
	_getnbr( "9" );
	_getnbr( "10" );
	_getnbr( "99" );
	_getnbr( "999" );
	_getnbr( "1000" );
	_getnbr( "1234567890" );
	_getnbr( "987654321" );
	_getnbr( "1000000000" );
	_getnbr( "2000000000" );
	_getnbr( " " );
	_getnbr( "ASD" );
	_getnbr( "ASD 21" );
	_getnbr( "21 ASD" );
	_getnbr( " 21 ASD" );
	_getnbr( "1234567890ASD" );
	_getnbr( "987654321 QWE" );
	_getnbr( "1000000000+111" );
	_getnbr( "2000000000-222" );
	_getnbr( "+0" );
	_getnbr( "+1" );
	_getnbr( "+9" );
	_getnbr( "+10" );
	_getnbr( "+1234567890ASD" );
	_getnbr( "+987654321 QWE" );
	_getnbr( "+1000000000+111" );
	_getnbr( "+2000000000-222" );
	_getnbr( "-9" );
	_getnbr( "-1" );
	_getnbr( "-0" );
	_getnbr( "-10" );
	_getnbr( "-1234567890" );
	_getnbr( "-987654321" );
	_getnbr( "-1000000000" );
	_getnbr( "-2000000000" );
	_getnbr( "++0" );
	_getnbr( "++1" );
	_getnbr( "++9" );
	_getnbr( "++10" );
	_getnbr( "+++0" );
	_getnbr( "+++1" );
	_getnbr( "+++9" );
	_getnbr( "+++10" );
	_getnbr( "---0" );
	_getnbr( "---1" );
	_getnbr( "---9" );
	_getnbr( "---10" );
	_getnbr( "++++0" );
	_getnbr( "++++1" );
	_getnbr( "++++9" );
	_getnbr( "++++10" );
	_getnbr( "-----0" );
	_getnbr( "-----1" );
	_getnbr( "-----9" );
	_getnbr( "-----10" );
	_getnbr( "-+-+-+-+999" );
	_getnbr( "+-+-+-+-1000" );
	_getnbr( "+-++++-+-1001" );
	_getnbr( "+--+-+-+++++-----+-++-+-+---------------+++++++-+----+-++-+++-1005" );
	_getnbr( "---+-+++------+++++----++--+-+---+++--+-+----+-++-+-+-+---+-34567890" );
	_getnbr( "---+-+++------+++++----++--+-+---+++--+-+----+-++-+-+-+---+-34567890QWE" );
	_getnbr( "---+-+++------+++++----++--+-+---+++--+-+----+-++-+-+-+---+-34567890 ASD" );
	_getnbr( "---+-+++------+++++----++--+-+---+++--+-+----+-++-+-+-+---+-34567890+111" );
	_getnbr( "2147483647" );
	_getnbr( "+2147483647" );
	_getnbr( "-2147483648" );

	_swap( 1000, 1001 );
	_swap( 2000, 2001 );
	_swap( 2147483647, -2147483648 );

	_strdup( "" );
	_strdup( " " );
	_strdup( "abc" );
	_strdup( "abcdefghijklmnopqrstuvwxyz" );
	_strdup( "abcdefghijklmnopqrstuvwxyz0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789abcdefghijklmnopqrstuvwxyz0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789abcdefghijklmnopqrstuvwxyz0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789abcdefghijklmnopqrstuvwxyz0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789abcdefghijklmnopqrstuvwxyz0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789abcdefghijklmnopqrstuvwxyz0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789abcdefghijklmnopqrstuvwxyz0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789" );

	_str_to_wordtab( "" );
	_str_to_wordtab( " " );
	_str_to_wordtab( "               " );
	_str_to_wordtab( "Hello" );
	_str_to_wordtab( "Hello world" );
	_str_to_wordtab( "a b c" );
	_str_to_wordtab( "a b c d" );
	_str_to_wordtab( "a b c d e f" );
	_str_to_wordtab( "a b c d e f g" );
	_str_to_wordtab( "a1 b2 c3 d4 e5 f6 g7" );
	_str_to_wordtab( "   a1  ---   b2 .... c3           d4 e5 f6 g7             " );
	_str_to_wordtab( "             a           b         " );
	_str_to_wordtab( "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed non risus. Suspendisse lectus tortor, dignissim sit amet, adipiscing nec, ultricies sed, dolor. Cras elementum ultrices diam. Maecenas ligula massa, varius a, semper congue, euismod non, mi. Proin porttitor, orci nec nonummy molestie, enim est eleifend mi, non fermentum diam nisl sit amet erat. Duis semper. Duis arcu massa, scelerisque vitae, consequat in, pretium a, enim. Pellentesque congue. Ut in risus volutpat libero pharetra tempor. Cras vestibulum bibendum augue. Praesent egestas leo in pede. Praesent blandit odio eu enim. Pellentesque sed dui ut augue blandit sodales. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Aliquam nibh. Mauris ac mauris sed pede pellentesque fermentum. Maecenas adipiscing ante non diam sodales hendrerit. Ut velit mauris, egestas sed, gravida nec, ornare ut, mi. Aenean ut orci vel massa suscipit pulvinar. Nulla sollicitudin. Fusce varius, ligula non tempus aliquam, nunc turpis ullamcorper nibh, in tempus sapien eros vitae ligula. Pellentesque rhoncus nunc et augue. Integer id felis. Curabitur aliquet pellentesque diam. Integer quis metus vitae elit lobortis egestas. Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Morbi vel erat non mauris convallis vehicula. Nulla et sapien. Integer tortor tellus, aliquam faucibus, convallis id, congue eu, quam. Mauris ullamcorper felis vitae erat. Proin feugiat, augue non elementum posuere, metus purus iaculis lectus, et tristique ligula justo vitae magna. Aliquam convallis sollicitudin purus. Praesent aliquam, enim at fermentum mollis, ligula massa adipiscing nisl, ac euismod nibh nisl eu lectus. Fusce vulputate sem at sapien. Vivamus leo. Aliquam euismod libero eu enim. Nulla nec felis sed leo placerat imperdiet. Aenean suscipit nulla in justo. Suspendisse cursus rutrum augue. Nulla tincidunt tincidunt mi. Curabitur iaculis, lorem vel rhoncus faucibus, felis magna fermentum augue, et ultricies lacus lorem varius purus. Curabitur eu amet." );
	return 0;
}
