/*
** my_strncat.c for libmy in /home/chartier/rendu/libmy/source
**
** Made by CHARTIER Rodolphe
** Login   <charti_r@etna-alternance.net>
**
** Started on  Thu Oct 12 17:33:54 2017 CHARTIER Rodolphe
** Last update Thu Oct 12 17:33:57 2017 CHARTIER Rodolphe
*/

#include "my.h"

char	*my_strncat(char *dest, char *src, int n)
{
  int	size;
  int	i;

  size = my_strlen(dest);
  i = 0;
  while (i < n && src[i])
    {
      dest[size + i] = src[i];
      ++i;
    }
  dest[size + i] = '\0';
  return (dest);
}
