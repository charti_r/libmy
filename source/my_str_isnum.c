/*
** my_str_isnum.c for libmy in /home/chartier/rendu/libmy/source
**
** Made by CHARTIER Rodolphe
** Login   <charti_r@etna-alternance.net>
**
** Started on  Thu Oct 12 17:34:42 2017 CHARTIER Rodolphe
** Last update Thu Oct 12 17:34:42 2017 CHARTIER Rodolphe
*/

#include "my.h"

int     my_str_isnum(char *str)
{
  int   i;

  i = 0;
  while (str[i] == '+' || str[i] == '-')
    ++i;
  while (str[i])
    {
      if (str[i] < '0' || str[i] > '9')
	return (0);
      ++i;
    }
  return (1);
}
