/*
** my_putnbr.c for libmy in /home/chartier/rendu/libmy/source
**
** Made by CHARTIER Rodolphe
** Login   <charti_r@etna-alternance.net>
**
** Started on  Thu Oct 12 17:39:49 2017 CHARTIER Rodolphe
** Last update Thu Oct 12 17:39:50 2017 CHARTIER Rodolphe
*/

#include "my.h"

static void	my_recur(int nb)
{
  if (nb >= 10)
    my_recur(nb / 10);
  my_putchar((nb % 10) + '0');
}

void	my_putnbr(int nb)
{
  if (nb < 0)
    {
      my_putchar('-');
      nb = -nb;
    }
  my_recur(nb);
}
