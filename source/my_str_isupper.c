/*
** my_str_isupper.c for libmy in /home/chartier/rendu/libmy/source
**
** Made by CHARTIER Rodolphe
** Login   <charti_r@etna-alternance.net>
**
** Started on  Thu Oct 12 17:34:31 2017 CHARTIER Rodolphe
** Last update Thu Oct 12 18:36:45 2017 CHARTIER Rodolphe
*/

#include "my.h"

int	my_str_isupper(char *str)
{
  int   i;

  i = 0;
  while (str[i])
    {
      if (str[i] < 'A' || str[i] > 'Z')
	return (0);
      ++i;
    }
  return (1);
}
