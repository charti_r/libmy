/*
** my_strcmp.c for libmy in /home/chartier/rendu/libmy/source
**
** Made by CHARTIER Rodolphe
** Login   <charti_r@etna-alternance.net>
**
** Started on  Thu Oct 12 17:35:28 2017 CHARTIER Rodolphe
** Last update Thu Oct 12 17:35:29 2017 CHARTIER Rodolphe
*/

#include "my.h"

int	my_strcmp(char *s1, char *s2)
{
  int	i;

  i = 0;
  while (s1[i] == s2[i] && (s1[i] && s2[i]))
    ++i;
  return (s1[i] - s2[i]);
}
