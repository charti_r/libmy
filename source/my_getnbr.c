/*
** my_getnbr.c for libmy in /home/chartier/rendu/libmy/source
**
** Made by CHARTIER Rodolphe
** Login   <charti_r@etna-alternance.net>
**
** Started on  Thu Oct 12 17:40:36 2017 CHARTIER Rodolphe
** Last update Thu Oct 12 17:40:36 2017 CHARTIER Rodolphe
*/

#include "my.h"

int	my_getnbr(char *str)
{
  int	i;
  int	nb;

  i = 0;
  nb = 0;
  while (str[i] == '+' || str[i] == '-')
    ++i;
  while (str[i] && str[i] >= '0' && str[i] <= '9')
    {
      nb = nb * 10;
      nb = nb + str[i] - '0';
      ++i;
    }
  return (nb * my_nbr_isneg(str));
}
