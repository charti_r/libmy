/*
** my_find_primeup.c for libmy in /home/chartier/rendu/libmy/source
**
** Made by CHARTIER Rodolphe
** Login   <charti_r@etna-alternance.net>
**
** Started on  Thu Oct 12 17:41:06 2017 CHARTIER Rodolphe
** Last update Thu Oct 12 17:50:56 2017 CHARTIER Rodolphe
*/

#include "my.h"

int	my_find_primeup(int nb)
{
  if (nb > 2 && !(nb % 2))
    ++nb;
  while (!my_is_prime(nb))
    nb += 2;
  return (nb);
}
