/*
** my_putnbr_base.c for libmy in /home/chartier/rendu/libmy/source
**
** Made by CHARTIER Rodolphe
** Login   <charti_r@etna-alternance.net>
**
** Started on  Thu Oct 12 17:39:54 2017 CHARTIER Rodolphe
** Last update Thu Oct 12 17:39:54 2017 CHARTIER Rodolphe
*/

#include "my.h"

void     my_putnbr_base(int nb, char *base)
{
  int   n_base;

  if (nb < 0)
    {
      my_putchar('-');
      nb = -nb;
    }
  n_base = my_strlen(base);
  if (nb >= n_base)
    {
      my_putnbr_base(nb / n_base, base);
      my_putnbr_base(nb % n_base, base);
    }
  else
    my_putchar(base[nb]);
}
