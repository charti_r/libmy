/*
** my_strncmp.c for libmy in /home/chartier/rendu/libmy/source
**
** Made by CHARTIER Rodolphe
** Login   <charti_r@etna-alternance.net>
**
** Started on  Thu Oct 12 17:33:46 2017 CHARTIER Rodolphe
** Last update Thu Oct 12 17:33:46 2017 CHARTIER Rodolphe
*/

#include "my.h"

int     my_strncmp(char *s1, char *s2, int n)
{
  int   i;

  i = 0;
  while ((s1[i] == s2[i] && i < n) && (s1[i] && s2[i]))
    ++i;
  return (s1[i] - s2[i]);
}
