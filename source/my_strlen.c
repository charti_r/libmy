/*
** my_strlen.c for libmy in /home/chartier/rendu/libmy/source
**
** Made by CHARTIER Rodolphe
** Login   <charti_r@etna-alternance.net>
**
** Started on  Thu Oct 12 17:34:12 2017 CHARTIER Rodolphe
** Last update Thu Oct 12 17:34:12 2017 CHARTIER Rodolphe
*/

#include "my.h"

int	my_strlen(char *str)
{
  int	i;

  if (!str)
    return (0);
  i = 0;
  while (str[i])
    ++i;
  return (i);
}
