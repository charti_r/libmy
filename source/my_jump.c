/*
** my_jump.c for libmy in /home/chartier/rendu/libmy/source
**
** Made by CHARTIER Rodolphe
** Login   <charti_r@etna-alternance.net>
**
** Started on  Thu Oct 12 17:40:25 2017 CHARTIER Rodolphe
** Last update Thu Oct 12 18:48:49 2017 CHARTIER Rodolphe
*/

#include "my.h"

int	my_jump_char(char *s, char c)
{
  int	i;

  i = 0;
  while (s[i] == c && s[i])
    ++i;
  return (i);
}

int	my_jump_space(char *s)
{
  return (my_jump_char(s, ' '));
}
