/*
** my_strlcpy.c for libmy in /home/chartier/rendu/libmy/source
**
** Made by CHARTIER Rodolphe
** Login   <charti_r@etna-alternance.net>
**
** Started on  Thu Oct 12 17:34:18 2017 CHARTIER Rodolphe
** Last update Thu Oct 12 18:31:45 2017 CHARTIER Rodolphe
*/

#include "my.h"

char	*my_strlcpy(char *dest, char *src, int l)
{
  int	i;

  i = 0;
  while (i < l && src[i])
    {
      dest[i] = src[i];
      ++i;
    }
  dest[i] = '\0';
  return (dest);
}
