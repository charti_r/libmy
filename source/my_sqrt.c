/*
** my_sqrt.c for libmy in /home/chartier/rendu/libmy/source
**
** Made by CHARTIER Rodolphe
** Login   <charti_r@etna-alternance.net>
**
** Started on  Thu Oct 12 17:35:49 2017 CHARTIER Rodolphe
** Last update Thu Oct 12 18:25:19 2017 CHARTIER Rodolphe
*/

#include "my.h"

int	my_sqrt(int nb)
{
  int	sqrt;

  sqrt = 0;
  while ((sqrt * sqrt) < nb)
    ++sqrt;
  return ((sqrt * sqrt) == nb ? sqrt : 0);
}
