/*
** my_sizeof_nbr.c for libmy in /home/chartier/rendu/libmy/source
**
** Made by CHARTIER Rodolphe
** Login   <charti_r@etna-alternance.net>
**
** Started on  Thu Oct 12 17:36:09 2017 CHARTIER Rodolphe
** Last update Thu Oct 12 18:16:02 2017 CHARTIER Rodolphe
*/

#include "my.h"

int	my_sizeof_nbr(int nb)
{
  int	len;

  len = (nb < 0) + 1;
  while (nb /= 10)
    ++len;
  return (len);
}
