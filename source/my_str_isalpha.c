/*
** my_str_isalpha.c for libmy in /home/chartier/rendu/libmy/source
**
** Made by CHARTIER Rodolphe
** Login   <charti_r@etna-alternance.net>
**
** Started on  Thu Oct 12 17:34:52 2017 CHARTIER Rodolphe
** Last update Thu Oct 12 18:46:11 2017 CHARTIER Rodolphe
*/

#include "my.h"

int	my_str_isalpha(char *str)
{
  int	i;

  i = 0;
  while (str[i])
    {
      if ((str[i] < 'a' || str[i] > 'z') && (str[i] < 'A' || str[i] > 'Z'))
	return (0);
      ++i;
    }
  return (1);
}
