/*
** get_next_line.c for libmy in /home/chartier/rendu/libmy/source
**
** Made by CHARTIER Rodolphe
** Login   <charti_r@etna-alternance.net>
**
** Started on  Thu Oct 12 17:41:48 2017 CHARTIER Rodolphe
** Last update Sat Oct 28 20:29:42 2017 CHARTIER Rodolphe
*/

#include "my.h"

char		*get_next_line(int fd)
{
  static char	*save = NULL;
  char		*buffer;
  int		sz_line;
  int		i;

  buffer = save;
  sz_line = my_sizeof_line(save);
  i = 0;
  while (sz_line == MY_NBUFF(i))
    {
      if ((buffer = my_str_realloc(buffer, MY_NBUFF(i + 1) + 1)) == NULL)
	return (NULL);
      if (my_read(fd, &buffer[MY_NBUFF(i)], MY_NBUFF(1)) == -1)
	return (NULL);
      sz_line = my_sizeof_line(buffer);
      ++i;
    }
  if (!buffer[0])
    {
      if (i)
	free(buffer);
      if (i && save)
	free(save);
      return (NULL);
    }
  if (i && save)
    free(save);
  buffer[sz_line] = '\0';
  save = my_strcpy_alloc(buffer, sz_line);
  return (save);
}


char		*get_next_line(int fd)
{
  static char	*save = NULL;
  static char   *ret = NULL;
  char          buff[MY_BUFF_SIZE + 1];
  int           sz_line;
  int           i;

  if (!save && (save = malloc(sizeof(*save) * (MY_BUFF_SIZE + 1))) == NULL)
    return (NULL);
  if (ret)
    {
      free(ret);
      ret = NULL;
    }

  sz_line = my_sizeof_line(save);
  i = 0;
  while (sz_line == MY_NBUFF(i))
    {
      if (my_read(fd, buff, MY_BUFF_SIZE) == -1)
        return (NULL);
      if ((save = realloc(save, sizeof(*save) * (MY_NBUFF(i + 1) + 1))) == NULL)
        return (NULL);
      if (my_strncpy(&save[MY_NBUFF(i), buff, MY_BUFF_SIZE]) == NULL)
        return (NULL);
      sz_line += my_sizeof_line(buff);
      ++i;
    }

  if ((ret = malloc(sizeof(*ret) * (sz_line + 1))) == NULL)
    return (NULL);
  return (my_strlcpy(ret, &save[0], sz_line));
}
