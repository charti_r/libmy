/*
** my_charcmp.c for libmy in /home/chartier/rendu/libmy/source
**
** Made by CHARTIER Rodolphe
** Login   <charti_r@etna-alternance.net>
**
** Started on  Thu Oct 12 17:41:41 2017 CHARTIER Rodolphe
** Last update Thu Oct 12 17:41:41 2017 CHARTIER Rodolphe
*/

#include "my.h"

int	my_charcmp(char c, char *str)
{
  int	i;

  i = 0;
  while (str[i])
    {
      if (c == str[i])
	return (1);
      ++i;
    }
  return (0);
}
