/*
** my_putstr.c for libmy in /home/chartier/rendu/libmy/source
**
** Made by CHARTIER Rodolphe
** Login   <charti_r@etna-alternance.net>
**
** Started on  Thu Oct 12 17:39:42 2017 CHARTIER Rodolphe
** Last update Thu Oct 12 17:39:42 2017 CHARTIER Rodolphe
*/

#include "my.h"

int	my_putstr(char *str)
{
  return (write(1, str, my_strlen(str)));
}
