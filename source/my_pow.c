/*
** my_pow.c for libmy in /home/chartier/rendu/libmy/source
**
** Made by CHARTIER Rodolphe
** Login   <charti_r@etna-alternance.net>
**
** Started on  Thu Oct 12 17:40:08 2017 CHARTIER Rodolphe
** Last update Thu Oct 12 17:40:09 2017 CHARTIER Rodolphe
*/

#include "my.h"

int	my_pow(int nb, int pow)
{
  if (pow < 0)
    return (0);
  if (!pow)
    return (1);
  if (pow == 1)
    return (nb);
  return (nb * my_pow(nb, pow - 1));
}
