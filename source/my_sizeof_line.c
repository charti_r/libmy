/*
** my_sizeof_line.c for libmy in /home/chartier/rendu/libmy/source
**
** Made by CHARTIER Rodolphe
** Login   <charti_r@etna-alternance.net>
**
** Started on  Thu Oct 12 17:36:20 2017 CHARTIER Rodolphe
** Last update Thu Oct 12 17:36:21 2017 CHARTIER Rodolphe
*/

#include "my.h"

int     my_sizeof_line(char *str)
{
  int   i;

  if (!str)
    return (0);
  i = 0;
  while (str[i] != '\n' && str[i])
    ++i;
  return (i);
}
