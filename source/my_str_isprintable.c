/*
** my_str_isprintable.c for libmy in /home/chartier/rendu/libmy/source
**
** Made by CHARTIER Rodolphe
** Login   <charti_r@etna-alternance.net>
**
** Started on  Thu Oct 12 17:34:36 2017 CHARTIER Rodolphe
** Last update Thu Oct 12 17:34:37 2017 CHARTIER Rodolphe
*/

#include "my.h"

int	my_str_isprintable(char *str)
{
  int	i;

  i = 0;
  while (str[i])
    {
      if (str[i] < 32 || str[i] > 126)
	return (0);
      ++i;
    }
  return (1);
}
