/*
** my_is_prime.c for libmy in /home/chartier/rendu/libmy/source
**
** Made by CHARTIER Rodolphe
** Login   <charti_r@etna-alternance.net>
**
** Started on  Thu Oct 12 17:40:32 2017 CHARTIER Rodolphe
** Last update Thu Oct 12 17:58:05 2017 CHARTIER Rodolphe
*/

#include "my.h"

int	my_is_prime(int nb)
{
  int	i;

  if (nb < 2)
    return (0);
  if (nb == 2)
    return (1);
  if (!(nb % 2))
    return (0);
  i = 3;
  while (i < nb)
    {
      if (!(nb % i))
	return (0);
      i += 2;
    }
  return (1);
}
