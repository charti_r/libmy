/*
** my_str_realloc.c for libmy in /home/chartier/rendu/libmy/source
**
** Made by CHARTIER Rodolphe
** Login   <charti_r@etna-alternance.net>
**
** Started on  Thu Oct 12 17:33:21 2017 CHARTIER Rodolphe
** Last update Sat Oct 28 20:24:24 2017 CHARTIER Rodolphe
*/

#include "my.h"

char	*my_str_realloc(char *str, int size)
{
  char	*new_str;

  if ((new_str = realloc(str, sizeof(*str) * (size + 1))) == NULL)
    return (NULL);
  if (str)
    return (my_strlcpy(new_str, str, size));
  return (new_str);
}
