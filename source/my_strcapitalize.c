/*
** my_strcapitalize.c for libmy in /home/chartier/rendu/libmy/source
**
** Made by CHARTIER Rodolphe
** Login   <charti_r@etna-alternance.net>
**
** Started on  Thu Oct 12 17:35:44 2017 CHARTIER Rodolphe
** Last update Sat Oct 28 19:19:59 2017 CHARTIER Rodolphe
*/

#include "my.h"

char	*my_strcapitalize(char *str)
{
  int	i;

  if (str[0] >= 'a' && str[0] <= 'z')
    str[0] = str[0] - 32;
  i = 0;
  while (str[i])
    {
      if (str[i] == ' ')
	if (str[i + 1] >= 'a' && str[i + 1] <= 'z')
	  str[i + 1] = str[i + 1] - 32;
      if ((str[i] >= 'A' && str[i] <= 'Z') || (str[i] >= 'a' && str[i] <= 'z'))
	if (str[i + 1] >= 'A' && str[i + 1] <= 'Z')
	  str[i + 1] = str[i + 1] + 32;
      ++i;
    }
  return (str);
}
