/*
** my_printf.c for libmy in /home/chartier/rendu/libmy/source
**
** Made by CHARTIER Rodolphe
** Login   <charti_r@etna-alternance.net>
**
** Started on  Thu Oct 12 17:40:03 2017 CHARTIER Rodolphe
** Last update Thu Oct 12 17:40:04 2017 CHARTIER Rodolphe
*/

#include "my.h"

static void	my_printf_flag(char flag, va_list list)
{
  if (flag == 'd' || flag == 'i')
    my_putnbr(va_arg(list, int));
  if (flag == 'u')
    my_putnbr(va_arg(list, unsigned int));
  if (flag == 'b')
    my_putnbr_base(va_arg(list, int), "01");
  if (flag == 'o')
    my_putnbr_base(va_arg(list, int), "01234567");
  if (flag == 'p' || flag == 'P')
    my_putstr("0x");
  if (flag == 'x' || flag == 'p')
    my_putnbr_base(va_arg(list, unsigned int), "0123456789abcdef");
  if (flag == 'X' || flag == 'P')
    my_putnbr_base(va_arg(list, unsigned int), "0123456789ABCDEF");
  if (flag == 'c')
    my_putchar(va_arg(list, int));
  if (flag == 's')
    my_putstr(va_arg(list, char*));
  if (flag == '%')
    my_putchar('%');
}

int		my_printf(const char *str, ...)
{
  va_list	list;
  int		i;

  va_start(list, str);
  i = 0;
  while (str[i])
    {
      if (str[i] == '%')
	my_printf_flag(str[++i], list);
      else
	my_putchar(str[i]);
      ++i;
    }
  va_end(list);
  return (i);
}
