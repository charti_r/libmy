/*
** my_strcat.c for libmy in /home/chartier/rendu/libmy/source
**
** Made by CHARTIER Rodolphe
** Login   <charti_r@etna-alternance.net>
**
** Started on  Thu Oct 12 17:35:33 2017 CHARTIER Rodolphe
** Last update Thu Oct 12 17:35:34 2017 CHARTIER Rodolphe
*/

#include "my.h"

char	*my_strcat(char *dest, char *src)
{
  int	size;
  int	i;

  size = my_strlen(dest);
  i = 0;
  while (src[i])
    {
      dest[size + i] = src[i];
      ++i;
    }
  dest[size + i] = '\0';
  return (dest);
}
