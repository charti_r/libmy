/*
** my_memset.c for libmy in /home/chartier/rendu/libmy/source
**
** Made by CHARTIER Rodolphe
** Login   <charti_r@etna-alternance.net>
**
** Started on  Thu Oct 12 17:40:19 2017 CHARTIER Rodolphe
** Last update Thu Oct 12 18:17:46 2017 CHARTIER Rodolphe
*/

#include "my.h"

char	*my_memset(char *s, char c, int n)
{
  int	i;

  i = 0;
  while (i < n)
    {
      s[i] = c;
      ++i;
    }
  return (s);
}
