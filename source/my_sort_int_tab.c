/*
** my_sort_int_tab.c for libmy in /home/chartier/rendu/libmy/source
**
** Made by CHARTIER Rodolphe
** Login   <charti_r@etna-alternance.net>
**
** Started on  Thu Oct 12 17:35:56 2017 CHARTIER Rodolphe
** Last update Thu Oct 12 17:35:56 2017 CHARTIER Rodolphe
*/

#include "my.h"

void	my_sort_int_tab(int *tab, int size)
{
  int	stop;
  int	i;

  stop = 0;
  while (!stop)
    {
      stop = 1;
      i = 0;
      while ((i + 1) < size)
	{
	  if (tab[i] > tab[i + 1])
	    {
	      my_swap_int(&tab[i], &tab[i + 1]);
	      stop = 0;
	    }
	  ++i;
	}
    }
}
