/*
** my_strupcase.c for libmy in /home/chartier/rendu/libmy/source
**
** Made by CHARTIER Rodolphe
** Login   <charti_r@etna-alternance.net>
**
** Started on  Thu Oct 12 17:32:36 2017 CHARTIER Rodolphe
** Last update Thu Oct 12 18:41:42 2017 CHARTIER Rodolphe
*/

#include "my.h"

char	*my_strupcase(char *str)
{
  int	i;

  i = 0;
  while (str[i])
    {
      if (str[i] >= 'a' && str[i] <= 'z')
	str[i] -= 32;
      ++i;
    }
  return (str);
}
