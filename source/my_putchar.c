/*
** my_putchar.c for libmy in /home/chartier/rendu/libmy/source
**
** Made by CHARTIER Rodolphe
** Login   <charti_r@etna-alternance.net>
**
** Started on  Thu Oct 12 17:39:59 2017 CHARTIER Rodolphe
** Last update Thu Oct 12 17:39:59 2017 CHARTIER Rodolphe
*/

#include "my.h"

int	my_putchar(char c)
{
  return (write(1, &c, 1));
}
