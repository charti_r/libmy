/*
** my_strlcat.c for libmy in /home/chartier/rendu/libmy/source
**
** Made by CHARTIER Rodolphe
** Login   <charti_r@etna-alternance.net>
**
** Started on  Thu Oct 12 17:34:26 2017 CHARTIER Rodolphe
** Last update Thu Oct 12 17:34:27 2017 CHARTIER Rodolphe
*/

#include "my.h"

char	*my_strlcat(char *dest, char *src, int l)
{
  int   size;
  int   i;

  size = my_strlen(dest);
  i = 0;
  if (l > size)
    {
      while (src[i] && (i + size) < l)
	{
	  dest[size + i] = src[i];
	  ++i;
	}
    }
  dest[l] = '\0';
  return (dest);
}
