/*
** my_sizeof_char.c for libmy in /home/chartier/rendu/libmy/source
**
** Made by CHARTIER Rodolphe
** Login   <charti_r@etna-alternance.net>
**
** Started on  Thu Oct 12 17:36:27 2017 CHARTIER Rodolphe
** Last update Thu Oct 12 17:36:27 2017 CHARTIER Rodolphe
*/

#include "my.h"

int	my_sizeof_char(char *str, char c)
{
  int	i;

  i = 0;
  while (str[i] != c && str[i])
    ++i;
  return (i);
}
