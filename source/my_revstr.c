/*
** my_revstr.c for libmy in /home/chartier/rendu/libmy/source
**
** Made by CHARTIER Rodolphe
** Login   <charti_r@etna-alternance.net>
**
** Started on  Thu Oct 12 17:36:36 2017 CHARTIER Rodolphe
** Last update Thu Oct 12 17:36:37 2017 CHARTIER Rodolphe
*/

#include "my.h"

char	*my_revstr(char *str)
{
  int	i;
  int	j;

  i = my_strlen(str);
  j = 0;
  while (j < i)
    my_swap_char(&str[--i], &str[j++]);
  return (str);
}
