/*
** my_strcpy_alloc.c for my_strcpy_alloc in /home/chartier/rendu/libmy/source
**
** Made by CHARTIER Rodolphe
** Login   <charti_r@etna-alternance.net>
**
** Started on  Sat Oct 28 20:56:59 2017 CHARTIER Rodolphe
** Last update Sat Oct 28 21:10:09 2017 CHARTIER Rodolphe
*/

#include "my.h"

char	*my_strcpy_alloc(char *str, int size)
{
  char	*ret;
  int	i;

  if ((ret = malloc(sizeof(*ret) * (size + 1))) == NULL)
    return (NULL);
  i = -1;
  while (str[++i])
    ret[i] = str[i];
  ret[i] = '\0';
  return (ret);
}
