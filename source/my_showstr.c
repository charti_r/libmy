/*
** my_showstr.c for libmy in /home/chartier/rendu/libmy/source
**
** Made by CHARTIER Rodolphe
** Login   <charti_r@etna-alternance.net>
**
** Started on  Thu Oct 12 17:36:31 2017 CHARTIER Rodolphe
** Last update Thu Oct 12 17:36:31 2017 CHARTIER Rodolphe
*/

#include "my.h"

int	my_showstr(char *str)
{
  int	i;

  i = 0;
  while (str[i])
    {
      if (str[i] < 32 || str[i] > 126)
	{
	  my_putchar('\\');
	  my_putnbr_base(str[i], "0123456789abcdef");
	}
      else
	my_putchar(str[i]);
      ++i;
    }
  return (0);
}
