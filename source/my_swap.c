/*
** my_swap.c for libmy in /home/chartier/rendu/libmy/source
**
** Made by CHARTIER Rodolphe
** Login   <charti_r@etna-alternance.net>
**
** Started on  Thu Oct 12 17:30:41 2017 CHARTIER Rodolphe
** Last update Thu Oct 12 17:31:09 2017 CHARTIER Rodolphe
*/

#include "my.h"

void	my_swap_int(int *a, int *b)
{
  int	c;

  c = *a;
  *a = *b;
  *b = c;
}

void	my_swap_char(char *a, char *b)
{
  char	c;

  c = *a;
  *a = *b;
  *b = c;
}

void	my_swap_float(float *a, float *b)
{
  float	c;

  c = *a;
  *a = *b;
  *b = c;
}
