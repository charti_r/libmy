/*
** my_strstr.c for libmy in /home/chartier/rendu/libmy/source
**
** Made by CHARTIER Rodolphe
** Login   <charti_r@etna-alternance.net>
**
** Started on  Thu Oct 12 17:33:14 2017 CHARTIER Rodolphe
** Last update Thu Oct 12 17:33:15 2017 CHARTIER Rodolphe
*/

#include "my.h"

char	*my_strstr(char *str, char *to_find)
{
  int	i;
  int	j;

  i = 0;
  while (str[i])
    {
      if (to_find[0] == str[i])
	{
	  j = 0;
	  while (to_find[j] == str[i + j] && (to_find[j] && str[i + j]))
	    ++j;
	  if (!to_find[j])
	    return (&str[i]);
	}
      ++i;
    }
  return (0);
}
