/*
** my_str_islower.c for libmy in /home/chartier/rendu/libmy/source
**
** Made by CHARTIER Rodolphe
** Login   <charti_r@etna-alternance.net>
**
** Started on  Thu Oct 12 17:34:46 2017 CHARTIER Rodolphe
** Last update Thu Oct 12 17:34:47 2017 CHARTIER Rodolphe
*/

#include "my.h"

int	my_str_islower(char *str)
{
  int   i;

  i = 0;
  while (str[i])
    {
      if (str[i] < 'a' || str[i] > 'z')
	return (0);
      ++i;
    }
  return (1);
}
