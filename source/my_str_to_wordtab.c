/*
** my_str_to_wordtab.c for libmy in /home/chartier/rendu/libmy/source
**
** Made by CHARTIER Rodolphe
** Login   <charti_r@etna-alternance.net>
**
** Started on  Thu Oct 12 17:32:59 2017 CHARTIER Rodolphe
** Last update Thu Oct 12 17:33:07 2017 CHARTIER Rodolphe
*/

#include "my.h"

static int	count_word(char *str, char sep)
{
  int		count;
  int		i;

  count = 1;
  i = my_jump_space(str);
  while (str[i])
    {
      if (str[i] == sep)
	{
	  while (str[i] == sep)
	    ++i;
	  i += my_jump_space(&str[i]);
	  if (str[i])
	    ++count;
	}
      else
	++i;
    }
  return (count);
}

static char	*fill_word(char *str, char sep)
{
  char		*word;
  int		size;

  size = my_sizeof_char(str, sep) - my_jump_space(str);
  if ((word = malloc(sizeof(*word) * (size + 1))) == NULL)
    return (NULL);
  return (my_strncpy(word, &str[my_jump_space(str)], size));
}

char	**my_str_to_wordtab(char *str, char sep)
{
  char	**wordtab;
  int	size;
  int	count;
  int	i;

  size = count_word(str, sep);
  if ((wordtab = malloc(sizeof(*wordtab) * (size + 1))) == NULL)
    return (NULL);
  count  = 0;
  i = my_jump_space(str);
  while (count < size)
    {
      if ((wordtab[count] = fill_word(&str[i], sep)) == NULL)
	return (NULL);
      i += my_jump_char(&str[i], sep);
      ++count;
    }
  wordtab[count] = NULL;
  return (wordtab);
}
