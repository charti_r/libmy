/*
** my_file.c for libmy in /home/chartier/rendu/libmy/source
**
** Made by CHARTIER Rodolphe
** Login   <charti_r@etna-alternance.net>
**
** Started on  Thu Oct 12 17:41:12 2017 CHARTIER Rodolphe
** Last update Sat Oct 13 15:09:19 2018 CHARTIER Rodolphe
*/

#include <string.h>

#include "my.h"

static int	_pute(char *str, char *arg)
{
  if (arg)
    my_fprintf(2, str, arg);
  else
    my_fprintf(2, str);
  return (-1);
}

int		my_open(const char *filename, const int mode)
{
  struct stat	s;
  int		file;

  file = 0;
  if (mode & O_CREAT)
    {
      if ((file = !access(filename, F_OK)) == 1 && stat(filename, &s) == -1)
	return (_pute("\"%s\": no such file or directory\n", filename));
    }
  else
    {
      if (access(filename, F_OK) == -1 || stat(filename, &s) == -1)
	return (_pute("\"%s\": no such file or directory\n", filename));
      file = 1;
    }
  if (file && !(s.st_mode & S_IFREG))
    return (_pute("\"%s\" is not a file\n", filename));
  if ((file = open(filename, mode,
		   S_IRUSR | S_IWUSR | S_IRGRP | S_IWGRP | S_IROTH)) == -1)
    return (_pute("\"%s\": no such file or directory\n", filename));
  return (file);
}

int	my_read(const int fd, char *str, const int size)
{
  int	ret;

  if ((ret = read(fd, str, size)) == -1)
    return (-1);
  str[ret] = '\0';
  return (ret);
}

char	*my_getfile(const char *filename, uint *filesize)
{
  char	*f;
  char	buff[MY_BUFF_SIZE + 1];
  int	fd;
  int	size;
  int	i;

  if (filesize)
    *filesize = 0;
  if ((fd = my_open(filename, O_RDONLY)) == -1)
    return (NULL);
  if ((f = malloc(sizeof(*f))) == NULL)
    return (NULL);
  f[0] = '\0';
  i = -1;
  while ((size = my_read(fd, buff, MY_BUFF_SIZE)) > 0)
  {
    if (filesize)
      *filesize += size;
    if ((f = realloc(f, sizeof(*f) * (MY_NBUFF(++i) + (size + 1)))) == NULL)
      return (NULL);
    if (my_strlcpy(&f[MY_NBUFF(i)], buff, size) == NULL)
      return (NULL);
  }
  close(fd);
  return (f);
}
