/*
** my_nbr_isneg.c for libmy in /home/chartier/rendu/libmy/source
**
** Made by CHARTIER Rodolphe
** Login   <charti_r@etna-alternance.net>
**
** Started on  Thu Oct 12 17:40:15 2017 CHARTIER Rodolphe
** Last update Thu Oct 12 17:40:16 2017 CHARTIER Rodolphe
*/

#include "my.h"

int	my_nbr_isneg(char *str)
{
  int	i;
  int	isneg;

  isneg = 1;
  i = 0;
  while (str[i] == '+' || str[i] == '-')
    {
      if (str[i] == '-')
	isneg = -isneg;
      ++i;
    }
  return (isneg);
}
