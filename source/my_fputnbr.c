/*
** my_fputnbr.c for libmy in /home/chartier/rendu/libmy/source
**
** Made by CHARTIER Rodolphe
** Login   <charti_r@etna-alternance.net>
**
** Started on  Thu Oct 12 17:40:49 2017 CHARTIER Rodolphe
** Last update Thu Oct 12 17:40:49 2017 CHARTIER Rodolphe
*/

#include "my.h"

void	my_fputnbr(int fd, int nb)
{
  if (nb < 0)
    {
      my_fputchar(fd, '-');
      nb = -nb;
    }
   if (nb >= 10)
    {
      my_fputnbr(fd, nb / 10);
      my_fputnbr(fd, nb % 10);
    }
  else
    my_fputchar(fd, nb + '0');
}
