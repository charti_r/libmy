/*
** my_getnbr_base.c for libmy in /home/chartier/rendu/libmy/source
**
** Made by CHARTIER Rodolphe
** Login   <charti_r@etna-alternance.net>
**
** Started on  Thu Oct 12 17:40:40 2017 CHARTIER Rodolphe
** Last update Thu Oct 12 17:40:40 2017 CHARTIER Rodolphe
*/

#include "my.h"

int	my_getnbr_base(char *str, char *base)
{
  int	i;
  int	j;
  int	nb;
  int	n_base;

  n_base = my_strlen(base);
  nb = 0;
  i = 0;
  while (str[i] == '+' || str[i] == '-')
    ++i;
  while (str[i])
    {
      j = 0;
      while (str[i] != base[j] && base[j])
	++j;
      if (j == n_base)
	return (nb * my_nbr_isneg(str));
      nb = nb * n_base;
      nb = nb + j;
      ++i;
    }
  return (nb * my_nbr_isneg(str));
}
