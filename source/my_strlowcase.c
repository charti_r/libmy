/*
** my_strlowcase.c for libmy in /home/chartier/rendu/libmy/source
**
** Made by CHARTIER Rodolphe
** Login   <charti_r@etna-alternance.net>
**
** Started on  Thu Oct 12 17:34:03 2017 CHARTIER Rodolphe
** Last update Thu Oct 12 18:44:10 2017 CHARTIER Rodolphe
*/

#include "my.h"

char	*my_strlowcase(char *str)
{
  int   i;

  i = 0;
  while (str[i])
    {
      if (str[i] >= 'A' && str[i] <= 'Z')
	str[i] += 32;
      ++i;
    }
  return (str);
}
