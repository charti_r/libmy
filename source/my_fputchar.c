/*
** my_fputchar.c for libmy in /home/chartier/rendu/libmy/source
**
** Made by CHARTIER Rodolphe
** Login   <charti_r@etna-alternance.net>
**
** Started on  Thu Oct 12 17:40:58 2017 CHARTIER Rodolphe
** Last update Thu Oct 12 17:40:58 2017 CHARTIER Rodolphe
*/

#include "my.h"

int	my_fputchar(int fd, char c)
{
  return (write(fd, &c, 1));
}
