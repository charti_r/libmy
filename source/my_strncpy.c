/*
** my_strncpy.c for libmy in /home/chartier/rendu/libmy/source
**
** Made by CHARTIER Rodolphe
** Login   <charti_r@etna-alternance.net>
**
** Started on  Thu Oct 12 17:33:30 2017 CHARTIER Rodolphe
** Last update Sat Oct 28 19:58:21 2017 CHARTIER Rodolphe
*/

#include "my.h"

char	*my_strncpy(char *dest, char *src, int n)
{
  int	i;

  i = 0;
  while (i < n && src[i])
    {
      dest[i] = src[i];
      ++i;
    }
  return (dest);
}
