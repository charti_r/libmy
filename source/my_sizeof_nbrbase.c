/*
** my_sizeof_nbrbase.c for libmy in /home/chartier/rendu/libmy/source
**
** Made by CHARTIER Rodolphe
** Login   <charti_r@etna-alternance.net>
**
** Started on  Thu Oct 12 17:36:15 2017 CHARTIER Rodolphe
** Last update Thu Oct 12 18:41:05 2017 CHARTIER Rodolphe
*/

#include "my.h"

int	my_sizeof_nbrbase(int nb, int base)
{
  int	len;

  if (!base)
    return (0);
  len = (nb < 0) + 1;
  while (nb /= base)
    ++len;
  return (len);
}
