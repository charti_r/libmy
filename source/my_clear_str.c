/*
** my_clear_str.c for libmy in /home/chartier/rendu/libmy/source
**
** Made by CHARTIER Rodolphe
** Login   <charti_r@etna-alternance.net>
**
** Started on  Thu Oct 12 17:41:36 2017 CHARTIER Rodolphe
** Last update Thu Oct 12 17:41:36 2017 CHARTIER Rodolphe
*/

#include "my.h"

char	*my_clear_str(char *str)
{
  int	i;

  i = 0;
  while (str[i])
    {
      str[i] = 0;
      ++i;
    }
  return (str);
}
