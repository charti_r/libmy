/*
** my_rand.c for libmy in /home/chartier/rendu/libmy/source
**
** Made by CHARTIER Rodolphe
** Login   <charti_r@etna-alternance.net>
**
** Started on  Thu Oct 12 17:36:46 2017 CHARTIER Rodolphe
** Last update Thu Oct 12 17:38:31 2017 CHARTIER Rodolphe
*/

/*
** (MAX_INT * GOLD_NUMBER) & 0x7FFFFFFF == (2147483647 * 1.61803398875) & 0x7FFFFFFF
*/
#define MY_MAGIC_NUMBER 1327217883

static void	my_genrand(int *ptr_nb, int *ptr_seed)
{
  static int	seed = 0;

  if (ptr_seed)
    seed = *ptr_seed;
  else
    {
      *ptr_nb = ((seed * MY_MAGIC_NUMBER) + 424242) & 0x7FFFFFFF;
      seed = *ptr_nb >> 1;
    }
}

void	my_srand(int seed)
{
  my_genrand((int *)(0), &seed);
}

int	my_rand(void)
{
  int	nb;

  my_genrand(&nb, (int *)(0));
  return (nb);
}
