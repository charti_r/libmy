/*
** my_fputnbr_base.c for libmy in /home/chartier/rendu/libmy/source
**
** Made by CHARTIER Rodolphe
** Login   <charti_r@etna-alternance.net>
**
** Started on  Thu Oct 12 17:40:53 2017 CHARTIER Rodolphe
** Last update Thu Oct 12 17:40:54 2017 CHARTIER Rodolphe
*/

#include "my.h"

void     my_fputnbr_base(int fd, int nb, char *base)
{
  int   n_base;

  if (nb < 0)
    {
      my_fputchar(fd, '-');
      nb = -nb;
    }
  n_base = my_strlen(base);
  if (nb >= n_base)
    {
      my_fputnbr_base(fd, nb / n_base, base);
      my_fputnbr_base(fd, nb % n_base, base);
    }
  else
    my_fputchar(fd, base[nb]);
}
