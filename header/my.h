/*
** my.h for libmy in /home/chartier/rendu/libmy/header
**
** Made by CHARTIER Rodolphe
** Login   <charti_r@etna-alternance.net>
**
** Started on  Thu Oct 12 17:14:59 2017 CHARTIER Rodolphe
** Last update Sat Oct 28 21:09:52 2017 CHARTIER Rodolphe
*/

#ifndef MY_H_
# define MY_H_

/*
** Includes
*/
# include <unistd.h>
# include <stdlib.h>
# include <stdarg.h>
# include <sys/types.h>
# include <sys/stat.h>
# include <fcntl.h>

/*
** Defines
*/
# define MY_MAX(a, b) ((a) > (b) ? (a) : (b))
# define MY_MIN(a, b) ((a) < (b) ? (a) : (b))
# define MY_ISNEG(a) ((a) < 0 ? 1 : 0)
# define MY_ISPOS(a) ((a) > 0 ? 1 : 0)
# define MY_ABS(a) ((a) > 0 ? (a) : (-a))

# define MY_BUFF_SIZE 4096
# define MY_NBUFF(n) (MY_BUFF_SIZE * (n))

/*
** Stream
*/
int	my_fprintf(int fd, const char *str, ...);
int	my_fputchar(int fd, char c);
int	my_fputstr(int fd, char *str);
void	my_fputnbr(int fd, int nb);
void	my_fputnbr_base(int fd, int nb, char *base);

int	my_printf(const char *str, ...);
int	my_putchar(char c);
int	my_putstr(char *str);
void	my_putnbr(int nb);
void	my_putnbr_base(int nb, char *base);

char	*get_next_line(int fd);

/*
** String Size
*/
int	my_strlen(char *str);
int	my_sizeof_line(char *str);
int	my_sizeof_char(char *str, char c);
int	my_sizeof_nbrbase(int nb, int base);
int	my_sizeof_nbr(int nb);

/*
** Manipulate String
*/
char	*my_memset(char *s, char c, int n);
char	*my_revstr(char *str);
char	*my_strcat(char *dest, char *src);
char	*my_strcpy(char *dest, char *src);
char	*my_strlcat(char *dest, char *src, int l);
char	*my_strlcpy(char *dest, char *src, int l);
char	*my_strncat(char *dest, char *src, int n);
char	*my_strncpy(char *dest, char *src, int n);

/*
** Parsing
*/
int	my_jump_char(char *s, char c);
int	my_jump_space(char *s);

int	my_charcmp(char c, char *str);
int	my_strcmp(char *s1, char *s2);
int	my_strncmp(char *s1, char *s2, int n);
int	my_str_isalpha(char *str);
int	my_str_isnum(char *str);
int	my_str_islower(char *str);
int	my_str_isupper(char *str);
int	my_str_isprintable(char *str);

/*
** Modify Display
*/
char	*my_strcapitalize(char *str);
char	*my_strlowcase(char *str);
char	*my_strupcase(char *str);
char	*my_strstr(char *str, char *to_find);
int	my_showstr(char *str);

/*
** Change / Sort Values
*/
void	my_swap_int(int *a, int *b);
void	my_swap_char(char *a, char *b);
void	my_swap_float(float *a, float *b);
void	my_sort_int_tab(int *tab, int size);
char	**my_str_to_wordtab(char *str, char sep);
char	*my_str_realloc(char *str, int size);
char	*my_strcpy_alloc(char *str, int size);

/*
** Maths
*/
int	my_getnbr(char *str);
int	my_getnbr_base(char *str, char *base);

int	my_nbr_isneg(char *str);
int	my_pow(int nb, int pow);
int	my_sqrt(int nb);
int	my_find_primeup(int nb);
int	my_is_prime(int nb);

#endif /* !MY_H_ */
